<?php

class UsersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	// public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			// 'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index','view','delete','update', 'index2'),
				'expression'=>function(){
					if(Yii::app()->user->isGuest || Users::model()->findByPk(Yii::app()->user->userId)->user_type == 2){
						return false;
					}

					return true;
				}
			),
			array('allow',
				'actions'=>array('profile'),
				'expression'=>function(){
					if(Yii::app()->user->isGuest || !Users::model()->findByPk(Yii::app()->user->userId)->user_type == 3){
						return false;
					}

					return true;
				}
			),

			array('deny'),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = Yii::app()->name;
		$model = $this->loadModel($id);

		if($model->user_type == 1){
			$modelAdmin = Administrators::model()->findByPk($model->employee_id);

			$userName = $modelAdmin->admin_firstname . " " . $modelAdmin->admin_lastname;
			$userType = 'Administrator';
		} else if($model->user_type == 2){
			$modelInstructor = Instructors::model()->findByPk($model->employee_id);

			$userName = $modelInstructor->instructor_firstname . " " . $modelInstructor->instructor_lastname;
			$userType = 'Instructor';
		}

		if(isset($_POST['Users']) && isset($_POST['btnSave'])){
			$model->attributes = $_POST['Users'];
			if($model->validate() && $model->save()){
				Yii::app()->user->setFlash('msg','Saved successfully');
				Yii::app()->user->setFlash('msgClass','alert alert-success');
				$this->refresh();
			}  else {
				Common::pre($model->getErrors());
				// Yii::app()->user->setFlash('msg','Error on saving changes');
				// Yii::app()->user->setFlash('msgClass','alert alert-error');
				// $this->refresh();
			}
		}

		$this->render('view',array(
			'model'=>$model,
			'userType'=>$userType,
			'userName'=>$userName,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Users']))
		{
			$model->attributes=$_POST['Users'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$this->pageTitle = Yii::app()->name.' | Manage Users';
		$modelUser=new Users('registration');
		$modelAdmin=new Administrators;
		$modelInstructor=new Instructors;
		$modelStudent=new Students;

		// clear any default values
		$modelUser->unsetAttributes();
		$modelAdmin->unsetAttributes();
		$modelInstructor->unsetAttributes();  
		$modelStudent->unsetAttributes();

		if(isset($_GET['Users'])){
			$modelUser->attributes=$_GET['Users'];
		}
		if(isset($_GET['Administrators'])){
			$modelAdmin->attributes=$_GET['Administrators'];
		}
		if(isset($_GET['Instructors'])){
			$modelInstructor->attributes=$_GET['Instructors'];
		}
		if(isset($_GET['Students'])){
			$modelStudent->attributes=$_GET['Students'];
		}

		// Creating a user for admin
		if(isset($_POST['Users']) && isset($_POST['Administrators']) && isset($_POST['btnSaveAdmin'])){
			$modelUser->attributes = $_POST['Users'];
			$modelAdmin->attributes = $_POST['Administrators'];
			$modelUser->user_status = 1;
			$modelUser->user_type = 1;
			$modelAdmin->id = $modelUser->employee_id;

			if($modelAdmin->validate() && $modelAdmin->save()){
				if($modelUser->validate() && $modelUser->save()){
					Yii::app()->user->setFlash('msg','Saved successfully');
					Yii::app()->user->setFlash('msgClass','alert alert-success');
					$this->refresh();
				}
			} else {
				Yii::app()->user->setFlash('msg','There was an error on saving');
				Yii::app()->user->setFlash('msgClass','alert alert-error');
				$this->refresh();
			}
		} else if(isset($_POST['Users']) && isset($_POST['btnSaveInstructor'])){
			$modelUser->attributes = $_POST['Users'];
			$modelUser->user_status = 1;
			$modelUser->user_type = 2;

			if($modelUser->validate() && $modelUser->save()){
				Yii::app()->user->setFlash('msg','Saved successfully');
				Yii::app()->user->setFlash('msgClass','alert alert-success');
				$this->refresh();
			} else {
				Yii::app()->user->setFlash('msg','There was an error on saving');
				Yii::app()->user->setFlash('msgClass','alert alert-error');
				$this->refresh();
			}
		} else if(isset($_POST['Users']) && isset($_POST['btnSaveStudent'])){
			$modelUser->attributes = $_POST['Users'];
			$modelUser->user_status = 1;
			$modelUser->user_type = 3;

			if($modelUser->validate() && $modelUser->save()){
				Yii::app()->user->setFlash('msg','Saved successfully');
				Yii::app()->user->setFlash('msgClass','alert alert-success');
				$this->refresh();
			} else {
				Yii::app()->user->setFlash('msg','There was an error on saving');
				Yii::app()->user->setFlash('msgClass','alert alert-error');
				$this->refresh();
			}
		}

		$this->render('admin',array(
			'modelUser'=>$modelUser,
			'modelAdmin'=>$modelAdmin,
			'modelInstructor'=>$modelInstructor,
			'modelStudent'=>$modelStudent,
		));
	}

	public function actionIndex2()
	{
		$modelStudent = new Students;
		$modelInstructor = new Instructors;

		if(isset($_POST['Students']) && isset($_POST['btn_savestudent'])){
			$modelStudent->attributes = $_POST['Students'];

			if($modelStudent->save()){
				Yii::app()->user->setFlash('msg','Saved successfully');
				Yii::app()->user->setFlash('msgClass','alert alert-success');
				$this->refresh();
			}
		}

		if(isset($_POST['Instructors']) && isset($_POST['btn_saveinstructor'])){
			$modelInstructor->attributes = $_POST['Instructors'];

			if($modelInstructor->save()){
				Yii::app()->user->setFlash('msg','Saved successfully');
				Yii::app()->user->setFlash('msgClass','alert alert-success');
				$this->refresh();
			}
		}

		$this->render('index2',array(
			'modelStudent'=>$modelStudent,
			'modelInstructor'=>$modelInstructor
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Users the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Users::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Users $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='users-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionProfile()
	{
		$usersModel = Users::model()->findByPk(Yii::app()->user->id);
		$model = Students::model()->findByAttributes(array('id'=>$usersModel->employee_id));
		if($usersModel->user_type == 3){
			$studentSubjects = StudentSubjects::getStudentSubjects($model->id);
		}

		$this->render('profile', array('model'=>$model,'studentSubjects'=>$studentSubjects));
	}
}
