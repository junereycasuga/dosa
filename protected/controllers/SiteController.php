<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		if(!Yii::app()->user->isGuest){
			$this->redirect(array('user/index'));
		}
		$this->pageTitle = Yii::app()->name.' | Sign in';

		$model = new Users('login');

		if(isset($_POST['Users']) && isset($_POST['btnLogin'])){
			$model->attributes = $_POST['Users'];

			if($model->validate() && $model->login()){
				$this->redirect(array('site/about'));
			} else {
				$model->user_password = '';
			}
		}

		$this->render('index',array('model'=>$model));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	// public function actionRegister()
	// {
	// 	$model=new Users;

	// 	if(isset($_POST['Users']) && isset($_POST['btnRegister'])){
	// 		$model->attributes = $_POST['Users'];

	// 		if($model->validate() && $model->save()){
	// 			Yii::app()->user->setFlash('msg','Saved successfully.');
	// 			Yii::app()->user->setFlash('msg','alert alert-success');
	// 		} else {
	// 			Yii::app()->user->setFlash('msg','There was an error on saving.');
	// 			Yii::app()->user->setFlash('msgClass','alert alert-error');
	// 		}
	// 	}
	// 	$this->render('register',array('model'=>$model));
	// }

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function actionLogin()
	{
		$this->redirect(array('site/index'));
	}

	public function actionAbout()
	{
		$this->pageTitle = Yii::app()->name." | About";
		$this->render('about');
	}
}
