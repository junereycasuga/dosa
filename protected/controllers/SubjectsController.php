<?php 

class SubjectsController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			// 'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('dropstudent,claimclasscard'),
				'expression'=>function(){
					if(Yii::app()->user->isGuest || Users::model()->findByPk(Yii::app()->user->id)->user_type == 3){
						return false;
					}

					return true;
				}
			),
			array('allow',
				'actions'=>array('index'),
				'expression'=>function(){
					if(Yii::app()->user->isGuest){
						return false;
					}

					return true;
				}
			),
			array('deny'),
		);
	}

	public function actionIndex()
	{
		$this->pageTitle = Yii::app()->name . " | Subjects";

		$empId = Users::model()->findByPk(Yii::app()->user->id)->employee_id;
		$instructorSubjects = InstructorSubjects::getAllSubjectsOfInstructor($empId);

		if(isset($_GET['subjectId'])){
			$students = StudentSubjects::getAllStudentsOfASubject($_GET['subjectId']);
			$studentsModel = new Students;
			
			$this->render('index',array(
				'instructorSubjects'=>$instructorSubjects,
				'students'=>$students,
				'studentsModel'=>$studentsModel,
			));
		} else {
			$this->render('index',array(
				'instructorSubjects'=>$instructorSubjects,
			));
		}


		
	}

	public function actionDropStudent()
	{
		$studentId = $_GET['studentId'];
		$subjectId = $_GET['subjectId'];

		$model =  new Classcards;
		$model->student_id = $studentId;
		$model->subject_id = $subjectId;
		$model->status = 0;

		$modelStudent = Students::model()->findByPk($studentId);

		if($model->validate() && $model->save()){
			// send mail
			$mail = new YiiMailer();
			$mail->IsSMTP();
			$mail->Host = 'smtp.gmail.com';
			$mail->Port = 465;
			$mail->SMTPAuth = true;
			$mail->SMTPSecure = 'ssl';
			$mail->Username = Yii::app()->params['adminEmail'];
			$mail->Password = Yii::app()->params['adminPassword'];
			$mail->setView('drop');
			$mail->render();
			$mail->ContentType = 'text/html';
			$mail->From = Yii::app()->params['adminEmail'];
			$mail->FromName = 'DOSA';
			$mail->Subject = 'Dropped Classcard';
			$mail->AddBCC($model->student_email);
			$mail->AddBCC($model->guardian_email);
			$mail->Send();

			Yii::app()->user->setFlash('msg','Student has been dropped succesfully');
			Yii::app()->user->setFlash('msgClass','alert alert-success');
			$this->redirect(array('subjects/index', 'subjectId'=>$subjectId));
		} else {
			Yii::app()->user->setFlash('msg','Error on dropping');
			Yii::app()->user->setFlash('msgClass','alert alert-error');
			$this->redirect(array('subjects/index', 'subjectId'=>$subjectId));
		}
	}

	public function actionClaimClasscard()
	{
		$studentId = $_GET['studentId'];
		$subjectId = $_GET['subjectId'];
		$model = Classcards::model()->findByAttributes(array('student_id'=>$studentId,'subject_id'=>$subjectId,'status'=>0));
		$model->date_claimed = date("Y-m-d H:i:s");
		$model->status  = 1;

		if($model->validate() && $model->save()){
			// send mail
			$mail = new YiiMailer();
			$mail->IsSMTP();
			$mail->Host = 'smtp.gmail.com';
			$mail->Port = 465;
			$mail->SMTPAuth = true;
			$mail->SMTPSecure = 'ssl';
			$mail->Username = Yii::app()->params['adminEmail'];
			$mail->Password = Yii::app()->params['adminPassword'];
			$mail->setView('claim');
			$mail->render();
			$mail->ContentType = 'text/html';
			$mail->From = Yii::app()->params['adminEmail'];
			$mail->FromName = 'DOSA';
			$mail->Subject = 'Claimed Classcard';
			$mail->AddBCC($model->student_email);
			$mail->AddBCC($model->guardian_email);
			$mail->Send();
			Yii::app()->user->setFlash('msg','Student classcard has been claimed succesfully');
			Yii::app()->user->setFlash('msgClass','alert alert-success');
			$this->redirect(array('subjects/index', 'subjectId'=>$subjectId));
		} else {
			Yii::app()->user->setFlash('msg','Error on claiming');
			Yii::app()->user->setFlash('msgClass','alert alert-error');
			$this->redirect(array('subjects/index', 'subjectId'=>$subjectId));
		}
	}
} 
?>