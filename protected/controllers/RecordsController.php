<?php 

class RecordsController extends Controller{

	public function accessRules()
	{
		return  array(
			array('allow',
				'actions'=>array('index', 'view', 'claim'),
				'expression'=>function(){
					if(Yii::app()->user->isGuest){
						return false;
					}

					return true;
				}
			),

			array('deny'),
		);
	}

	public function actionIndex()
	{
		$this->pageTitle = Yii::app()->name." | Student Records";
		$classcards = new Classcards;
		$collectedIds = new CollectedId;

		$classcards->unsetAttributes();
		$collectedIds->unsetAttributes();

		if(isset($_GET['Classcards'])){
			$classcards->attributes=$_GET['Classcards'];
		}

		if(isset($_GET['CollectedId'])){
			$collectedIds->attributes=$_GET['CollectedId'];
		}

		if(isset($_POST['Classcards']) && isset($_POST['btnDrop'])){
			$classcards->attributes = $_POST['Classcards'];
			$classcards->status = 0;

			if($classcards->validate() && $classcards->save()){
				Yii::app()->user->setFlash('msg','Student classcard successfully dropped');
				Yii::app()->user->setFlash('msgClass','alert alert-success');
			} else {
				Yii::app()->user->setFlash('msg','Dropping of classcard failed.');
				Yii::app()->user->setFlash('msgClass','alert alert-danger');
			}
		}

		if(isset($_POST['CollectedId']) && isset($_POST['btnCollect'])){
			$collectedIds->attributes = $_POST['CollectedId'];
			$collectedIds->status = 0;
			if($collectedIds->validate() && $collectedIds->save()){
				Yii::app()->user->setFlash('msg','Student ID collected successfully');
				Yii::app()->user->setFlash('msgClass','alert alert-success');
			} else {
				Yii::app()->user->setFlash('msg','Collecting of Student ID failed.');
				Yii::app()->user->setFlash('msgClass','alert alert-danger');
			}
		}

		$this->render('index', array(
			'classcards'=>$classcards,
			'collectedIds'=>$collectedIds,
		));
	}

	public function actionView($id)
	{
		$this->pageTitle = Yii::app()->name . " | Record Details";

		$type = $_GET['type'];
		if($type == "classcard"){
			$model = Classcards::model()->findByPk($id);
		} else if($type == "studentId"){
			$model = CollectedId::model()->findByPk($id);
		}
		$students = new Students;

		$this->render('view', array(
			'model'=>$model,
			'students'=>$students,
		));
	}

	public function actionClaim($id, $type)
	{
		if($type == 'classcard'){
			$claim = Classcards::claim($id);
		} else if($type == 'studentId'){
			$claim = CollectedId::claim($id);
		}

		if($claim){
			Yii::app()->user->setFlash('msg','Class card claimed successfully');
			Yii::app()->user->setFlash('msgClass','alert alert-success');
			$this->redirect(array('records/index'));
		} else {
			Yii::app()->user->setFlash('msg','Class card claiming failed');
			Yii::app()->user->setFlash('msgClass','alert alert-danger');
			$this->redirect(array('records/index'));
		}
	}
}
?>