<?php

/**
 * This is the model class for table "collected_id".
 *
 * The followings are the available columns in table 'collected_id':
 * @property integer $id
 * @property integer $student_id
 * @property string $reason
 * @property string $date_collected
 * @property string $date_claimed
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Students $student
 */
class CollectedId extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CollectedId the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'collected_id';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('student_id, reason, status', 'required'),
			array('student_id', 'numerical', 'integerOnly'=>true),
			array('reason, date_claimed', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, student_id, reason, date_collected, date_claimed, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'student' => array(self::BELONGS_TO, 'Students', 'student_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'student_id' => 'Student',
			'reason' => 'Reason',
			'date_collected' => 'Date Collected',
			'date_claimed' => 'Date Claimed',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('student_id',$this->student_id);
		$criteria->compare('reason',$this->reason,true);
		$criteria->compare('date_collected',$this->date_collected,true);
		$criteria->compare('date_claimed',$this->date_claimed,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function allCollectedId()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "status = 0";

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function claim($id)
	{
		$model = self::model()->findByPk($id);
		$model->date_claimed = date("Y-m-d H:i:s"); 
		$model->status = 1;

		if($model->save()){
			return true;
		}
	}

	public function getStudentName() {
		$model = Students::model()->findByPk($this->student_id);

		return $model->student_firstname . " " . $model->student_lastname;
	}

	public static function getClaimedUnclaimed(){
		return array(
			array('id'=>'0','title'=>'Not Claimed'),
			array('id'=>'1','title'=>'Claimed')
		);
	}
}