<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property integer $employee_id
 * @property string $username
 * @property string $user_password
 * @property integer $user_status
 * @property integer $user_type
 * @property string $user_created_at
 *
 * The followings are the available model relations:
 * @property Instructors $employee
 */
class Users extends CActiveRecord
{
	public $rememberMe;
	private $_identity;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			// required fields on registration
			array('employee_id, username, user_password, user_type', 'required', 'on'=>'registration'),
			// required fields on login
			array('username, user_password', 'required', 'on'=>'login'),
			array('id, employee_id, user_status, user_type', 'numerical', 'integerOnly'=>true),
			array('username', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, employee_id, username, user_password, user_status, user_type, user_created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			// 'employee' => array(self::BELONGS_TO, 'Instructors', 'employee_id'),
			'employee' => array(self::BELONGS_TO, 'Administrators', 'employee_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'employee_id' => 'Employee ID',
			'username' => 'Username',
			'user_password' => 'Password',
			'user_status' => 'Status',
			'user_type' => 'Type',
			'user_created_at' => 'Created At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria(array('condition'=>'user_type != 3'));

		$criteria->compare('id',$this->id);
		$criteria->compare('employee_id',$this->employee_id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('user_password',$this->user_password,true);
		$criteria->compare('user_status',$this->user_status, true);
		$criteria->compare('user_type',$this->user_type, true);
		$criteria->compare('user_created_at',$this->user_created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function beforeSave(){
		$TPassword = new TPassword();
		$this->user_password = $TPassword->hash($this->user_password);

		return true;
	}

	public function login(){
		$TPassword = new TPassword();
		$this->setAttribute('user_password', $TPassword->hash($this->user_password));

		$this->rememberMe = 0;
		if(isset($_POST['Users']['rememberMe'])){
			$this->rememberMe = $_POST['Users']['rememberMe'];
		}

		// authenticate user
		if($this->_identity===null){
			$this->_identity = new UserIdentity($this->username,$this->user_password);
			$this->_identity->authenticate();
		}

		if($this->_identity->errorCode === UserIdentity::ERROR_NONE){
			$duration = $this->rememberMe ? 3600*24*30 : 0;
			Yii::app()->user->login($this->_identity, $duration);
			return true;
		} else {
			if($this->_identity->errorCode == -1){
				Yii::app()->user->setFlash('msg','Not allowed to login');
				Yii::app()->user->setFlash('msgClass','alert alert-error');
			} else {
				Yii::app()->user->setFlash('msg','Invalid username/password');
				Yii::app()->user->setFlash('msgClass','alert alert-error');
			}
		}
	}

	public function getUsersName(){
		if($this->user_type == 1){
			$model = Administrators::model()->findByPk($this->employee_id);
			$name = $model->admin_firstname . " " . $model->admin_lastname;

		} else if($this->user_type == 2){
			$model = Instructors::model()->findByPk($this->employee_id);
			$name = $model->instructor_firstname . " " . $model->instructor_lastname;
		} else if($this->user_type == 3){
			$model = Instructors::model()->findByPk($this->employee_id);
			$name = $model->student_firstname . " " . $model->student_lastname;
		}

		return $name;
	}

	public static function getUsername($id, $type){
		$model = self::model()->findByAttributes(array('employee_id'=>$id,'user_type'=>$type));
		return $model->username;
	}
}