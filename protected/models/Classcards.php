<?php

/**
 * This is the model class for table "classcards".
 *
 * The followings are the available columns in table 'classcards':
 * @property integer $id
 * @property integer $student_id
 * @property integer $subject_id
 * @property string $date_dropped
 * @property string $date_claimed
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Students $subject
 */
class Classcards extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Classcards the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'classcards';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('student_id, subject_id, status', 'required'),
			array('student_id, subject_id', 'numerical', 'integerOnly'=>true),
			array('date_claimed', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, student_id, subject_id, date_dropped, date_claimed, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'subject' => array(self::BELONGS_TO, 'Students', 'subject_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'student_id' => 'Student',
			'subject_id' => 'Subject',
			'date_dropped' => 'Date Dropped',
			'date_claimed' => 'Date Claimed',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('student_id',$this->student_id);
		$criteria->compare('subject_id',$this->subject_id);
		$criteria->compare('date_dropped',$this->date_dropped,true);
		$criteria->compare('date_claimed',$this->date_claimed,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function allDroppedClasscards()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "status = 0";

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function claim($id)
	{
		$model = self::model()->findByPk($id);
		$model->date_claimed = date("Y-m-d H:i:s"); 
		$model->status = 1;

		if($model->save()){
			return true;
		}
	}

	public function getStudentName() {
		$model = Students::model()->findByPk($this->student_id);

		return $model->student_firstname . " " . $model->student_lastname;
	}

	public function getSubjectTitle(){
		$model = InstructorSubjects::model()->findByPk($this->subject_id);

		$subjectModel = Subjects::getSubjectTitle($model->subject_id);

		return $subjectModel;
	}

	public static function checkIfStudentIsDropped($studentId, $subjectId)
	{
		$model = self::model()->findByAttributes(array('student_id'=>$studentId, 'subject_id'=>$subjectId,'status'=>0));

		if($model){
			return "Dropped";
		} else {
			return "Not Dropped";
		}
	}

	public static function countDropsOfAStudentPerSubject($studentId, $subjectId)
	{
		$dropCount = Yii::app()->db->createCommand()
			->select('COUNT(*)')
			->from('classcards t1')
			->where('student_id = :studentId', array(':studentId'=>$studentId))
			->andWhere('subject_id = :subjectId', array(':subjectId'=>$subjectId))
			->queryScalar();

		return $dropCount;
	}

	public static function getClaimedUnclaimed(){
		return array(
			array('id'=>'0','title'=>'Not Claimed'),
			array('id'=>'1','title'=>'Claimed')
		);
	}
}