<?php

/**
 * This is the model class for table "students".
 *
 * The followings are the available columns in table 'students':
 * @property integer $id
 * @property string $student_firstname
 * @property string $student_middlename
 * @property string $student_lastname
 * @property integer $student_course_id
 * @property integer $student_year_level
 * @property string $student_address
 * @property string $studetn_contact_no
 * @property string $guardian_name
 * @property string $guardian_contact_no
 *
 * The followings are the available model relations:
 * @property Classcards[] $classcards
 * @property CollectedId[] $collecteds
 * @property StudentSubjects[] $studentSubjects
 * @property Courses $studentCourse
 */
class Students extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Students the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'students';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('student_firstname, student_middlename, student_lastname, student_course_id, student_year_level, student_email', 'required'),
			array('student_course_id, student_year_level', 'numerical', 'integerOnly'=>true),
			array('student_firstname, student_middlename, student_lastname, student_email, studetn_contact_no, guardian_name, guardian_email, guardian_contact_no,', 'length', 'max'=>100),
			array('student_address', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, student_firstname, student_middlename, student_lastname, student_course_id, student_year_level, student_address, student_email, studetn_contact_no, guardian_name, guardian_contact_no, guardian_email', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'classcards' => array(self::HAS_MANY, 'Classcards', 'subject_id'),
			'collecteds' => array(self::HAS_MANY, 'CollectedId', 'student_id'),
			'studentSubjects' => array(self::HAS_MANY, 'StudentSubjects', 'student_id'),
			'studentCourse' => array(self::BELONGS_TO, 'Courses', 'student_course_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'student_firstname' => 'Student Firstname',
			'student_middlename' => 'Student Middlename',
			'student_lastname' => 'Student Lastname',
			'student_course_id' => 'Student Course',
			'student_year_level' => 'Student Year Level',
			'student_address' => 'Student Address',
			'studetn_contact_no' => 'Studetn Contact No',
			'guardian_name' => 'Guardian Name',
			'guardian_contact_no' => 'Guardian Contact No',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('student_firstname',$this->student_firstname,true);
		$criteria->compare('student_middlename',$this->student_middlename,true);
		$criteria->compare('student_lastname',$this->student_lastname,true);
		$criteria->compare('student_course_id',$this->student_course_id);
		$criteria->compare('student_year_level',$this->student_year_level);
		$criteria->compare('student_address',$this->student_address,true);
		$criteria->compare('studetn_contact_no',$this->studetn_contact_no,true);
		$criteria->compare('guardian_name',$this->guardian_name,true);
		$criteria->compare('guardian_contact_no',$this->guardian_contact_no,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getStudentName($id=NULL)
	{
		if($id){
			$firstname = self::model()->findByPk($id)->student_firstname;
			$lastname = self::model()->findByPk($id)->student_lastname;

			return $firstname . " " . $lastname;
		} else {
			return $this->id . " - " . $this->student_firstname . " " . $this->student_lastname;
		}
	}

	public static function getStudents()
	{
		$students = self::model()->findAll();
		$list = CHtml::listData($students,'id','studentName');

		return $list;
	}
	
	public static function getYearLevel($yearLevel)
	{
		if($yearLevel == 1){
			return "1st Year";
		} else if($yearLevel == 2){
			return "2nd Year";
		} else if($yearLevel == 3){
			return "3rd Year";
		} else if($yearLevel == 4){
			return "4th Year";
		} else if($yearLevel == 5){
			return "5th Year";
		}
	}

	public function getCourse()
	{
		return Courses::getCourse($this->student_course_id);
	}
}