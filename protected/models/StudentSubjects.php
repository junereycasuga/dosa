<?php

/**
 * This is the model class for table "student_subjects".
 *
 * The followings are the available columns in table 'student_subjects':
 * @property integer $student_id
 * @property integer $subject_id
 *
 * The followings are the available model relations:
 * @property Students $student
 * @property InstructorSubjects $subject
 */
class StudentSubjects extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return StudentSubjects the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'student_subjects';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('student_id, subject_id', 'required'),
			array('student_id, subject_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('student_id, subject_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'student' => array(self::BELONGS_TO, 'Students', 'student_id'),
			'subject' => array(self::BELONGS_TO, 'InstructorSubjects', 'subject_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'student_id' => 'Student',
			'subject_id' => 'Subject',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('student_id',$this->student_id);
		$criteria->compare('subject_id',$this->subject_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function getStudentSubjects($id)
	{
		$subject = Yii::app()->db->createCommand()
			->select('t1.student_id, t2.subject_title')
			->from('student_subjects t1')
			->leftJoin('subjects t2','t2.id = t1.subject_id')
			->where('t1.student_id = :studentId', array(':studentId'=>$id))
			->queryAll();

		return $subject;
	}

	public static function getALlStudentsOfASubject($id)
	{
		$students = Yii::app()->db->createCommand()
			->select('t1.student_id')
			->from('student_subjects t1')
			->where('t1.subject_id = :subjectId', array(':subjectId'=>$id))
			->queryAll();

		return $students;
	}
}