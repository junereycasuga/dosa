<?php

/**
 * This is the model class for table "instructor_subjects".
 *
 * The followings are the available columns in table 'instructor_subjects':
 * @property integer $id
 * @property integer $instructor_id
 * @property integer $subject_id
 *
 * The followings are the available model relations:
 * @property Instructors $instructor
 * @property Subjects $subject
 * @property StudentSubjects[] $studentSubjects
 */
class InstructorSubjects extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InstructorSubjects the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'instructor_subjects';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('instructor_id, subject_id', 'required'),
			array('instructor_id, subject_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, instructor_id, subject_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'instructor' => array(self::BELONGS_TO, 'Instructors', 'instructor_id'),
			'subject' => array(self::BELONGS_TO, 'Subjects', 'subject_id'),
			'studentSubjects' => array(self::HAS_MANY, 'StudentSubjects', 'subject_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'instructor_id' => 'Instructor',
			'subject_id' => 'Subject',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('instructor_id',$this->instructor_id);
		$criteria->compare('subject_id',$this->subject_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function getAllSubjectsOfInstructor($id)
	{
		$subject = Yii::app()->db->createCommand()
			->select('t1.*')
			->from('instructor_subjects t1')
			->where('t1.instructor_id = :instructorId', array(':instructorId'=>$id))
			->queryAll();

		return $subject;
	}

	public static function getInstructorId($id)
	{
		return Instructors::model()->findByPk($id);
	}
}