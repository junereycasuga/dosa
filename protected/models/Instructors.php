<?php

/**
 * This is the model class for table "instructors".
 *
 * The followings are the available columns in table 'instructors':
 * @property integer $id
 * @property string $instructor_firstname
 * @property string $instructor_lastname
 *
 * The followings are the available model relations:
 * @property InstructorSubjects[] $instructorSubjects
 * @property Users[] $users
 */
class Instructors extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Instruct the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'instructors';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('instructor_firstname, instructor_lastname, instructor_email', 'required'),
			array('id', 'numerical', 'integerOnly'=>true),
			array('instructor_firstname, instructor_lastname, instructor_email', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, instructor_firstname, instructor_lastname, instructor_email', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'instructorSubjects' => array(self::HAS_MANY, 'InstructorSubjects', 'instructor_id'),
			'users' => array(self::HAS_MANY, 'Users', 'employee_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'instructor_firstname' => 'Instructor Firstname',
			'instructor_lastname' => 'Instructor Lastname',
			'instructor_email' => 'Instructor Email',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('instructor_firstname',$this->instructor_firstname,true);
		$criteria->compare('instructor_lastname',$this->instructor_lastname,true);
		$criteria->compare('instructor_email',$this->instructor_email,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getInstructorName($id = NULL){
		if($id){
			$firstname = self::model()->findByPk($id)->instructor_firstname;
			$lastname = self::model()->findByPk($id)->instructor_lastname;

			return $firstname . " " . $lastname;
		} else {
			return $this->instructor_firstname." ".$this->instructor_lastname;
		}
	}

	public static function getInstructors(){
		$instructors = self::model()->findAll();
		$list = CHtml::listData($instructors,'id','instructorName');

		return $list;
	}
}