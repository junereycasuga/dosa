<?php

class TagbondMail {

	public function sendTagbondMail($settings=array(), $data=array(), $response=array(),$multiple = null){
		$mail = new YiiMailer();
		$mail->IsSMTP();
		$mail->Host = 'smtp.gmail.com';
		$mail->Port = 465;
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'ssl';
		$mail->Username = Yii::app()->params['webEmail'];
		$mail->Password = Yii::app()->params['adminEmailPassword'];
		$mail->setView($settings['view']);
		$mail->setData($data);
		$mail->render();
		$mail->ContentType = 'text/html';
		$mail->From = Yii::app()->params['webEmail'];
		$mail->FromName = Yii::app()->name;
		$mail->Subject = $settings['subject'];
		
		if($data['pdfFile']){
			$mail->AddAttachment($data['pdfFile'].".pdf");	
		}
		if($multiple){
			foreach ($multiple as $key => $value) {
				$mail->AddBCC($value);
			}
		}else{
			$mail->AddAddress($data['email']);
		}
		if ($mail->Send()){
			return true;
		} else {
			return false;
		}
	
	}
}
?>