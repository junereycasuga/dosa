<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;
	const ERROR_USER_INACTIVE = -1;
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$users= Users::model()->findByAttributes(array('username'=>$this->username));

		if($users == null){
			return $this->errorCode = self::ERROR_USERNAME_INVALID;
		} else if ($users->user_password != $this->password){
			return $this->errorCode = self::ERROR_PASSWORD_INVALID;
		} else if ($users->user_status != 1){
			return $this->errorCode = self::ERROR_USER_INACTIVE;
		}

		// setting of session
		$this->_id = $users->id;
		$this->setState('userId', $users->id);
		$this->setState('username', $users->username);

		 if($users->user_type == 1){
			$admin = Administrators::model()->findByAttributes(array('id'=>$users->employee_id));
			$this->setState('userFirstname', $admin->admin_firstname);
			$this->setState('userLastname', $admin->admin_lastname);
		} else if($users->user_type == 2) {
			$instructor = Instructors::model()->findByAttributes(array('id'=>$users->employee_id));
			$this->setState('userFirstname', $instructor->instructor_firstname);
			$this->setState('userLastname', $instructor->instructor_lastname);
		} else if($users->user_type ==3) {
			$student = Students::model()->findByAttributes(array('id'=>$users->employee_id));
			$this->setState('userFirstname', $student->student_firstname);
			$this->setState('userLastname', $student->student_lastname);
		}
		
		$this->errorCode=self::ERROR_NONE;
	}

	public function getId(){
		return $this->_id;
	}
}