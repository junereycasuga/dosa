<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'collect-id-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal',
	),
)); ?>

	<div class="control-group">
		<?php echo $form->labelEx($collectedIds,'student_id', array('class'=>'control-label')); ?>
		<div class="controls">
			<?php echo $form->dropDownList($collectedIds,'student_id', Students::getStudents(),array('prompt'=>'Select Student','id'=>'collectId','class'=>'span3')); ?>
			<?php echo $form->error($collectedIds,'student_id'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($collectedIds,'reason',array('class'=>'control-label')); ?>
		<div class="controls">
			<?php echo $form->textArea($collectedIds,'reason',array('class'=>'span3')); ?>
			<?php echo $form->error($collectedIds,'reason'); ?>
		</div>
	</div>

	<div class="control-group">
		<div class="controls">
			<input type="submit" value="Collect" class="btn btn-danger" name="btnCollect">
		</div>
	</div>
<?php $this->endWidget(); ?>

<script>
	$('#collectId').select2();
</script>