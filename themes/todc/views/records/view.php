<title><?php echo $this->pageTitle; ?></title>
<div class="row-fluid">
	<div class="span9">
		<table class="table table-striped">
			<tbody>
				<tr>
					<th>Student ID</th>
					<td><?php echo $model->student_id; ?></td>
				</tr>
				<tr>
					<th>Name</th>
					<td><?php echo $students->getStudentName($model->student_id); ?></td>
				</tr>
				<?php if($_GET['type'] == 'classcard'){ ?>
				<tr>
					<th>Subject</th>
					<td><?php echo Subjects::model()->findByPk($model->subject_id)->subject_title; ?></td>
				</tr>
				<tr>
					<th>Date Dropped</th>
					<td><?php echo $model->date_dropped ?></td>
				</tr>
				<?php } ?>
				<?php if($_GET['type'] == 'studentId'){ ?>
				<tr>
					<th>Date Collected</th>
					<td><?php echo $model->date_collected; ?></td>
				</tr>
				<?php } ?>
				<tr>
					<th>Date Claimed</th>
					<td><?php echo $model->date_claimed; ?></td>
				</tr>
				<tr>
					<th>Status</th>
					<td>
						<?php if($model->status == '0'){
							echo "Not claimed";
						} else {
							echo "Claimed";
						} ?>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="span3">
		<div class="well">
			<?php if($_GET['type'] == 'classcard'){ ?>
				<a href="<?php echo Yii::app()->createUrl("records/claim", array("id"=>$model->id, "type"=>"classcard")); ?>" class="btn btn-danger btn-large" role="button">Claim</a>
			<?php } else if($_GET['type'] == 'studentId'){ ?>
				<a href="<?php echo Yii::app()->createUrl("records/claim", array("id"=>$model->id, "type"=>"studentId")); ?>" class="btn btn-danger btn-large" role="button">Claim</a>
			<?php } ?>
		</div>
	</div>
</div>