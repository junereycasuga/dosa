<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'drop-classcard-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal',
	),
)); ?>

	<div class="control-group">
		<?php echo $form->labelEx($classcards, 'student_id',array('class'=>'control-label')); ?>
		<div class="controls">
			<?php echo $form->dropDownList($classcards,'student_id', Students::getStudents(),array('prompt'=>'Select Student','id'=>'dropStudent','class'=>'span3')); ?>
			<?php echo $form->error($classcards,'student_id'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($classcards,'subject_id', array('class'=>'control-label')); ?>
		<div class="controls">
			<?php echo $form->dropDownList($classcards,'subject_id',Subjects::getSubjects(), array('prompt'=>'Select Subject','id'=>'dropSubject','class'=>'span3')); ?>
			<?php echo $form->error($classcards,'subject_id') ?>
		</div>
	</div>

	<div class="control-group">
		<div class="controls">
			<input type="submit" value="Drop" class="btn btn-danger" name="btnDrop">
		</div>
	</div>
<?php $this->endWidget(); ?>

<script>
	$('#dropStudent, #dropSubject').select2();
</script>