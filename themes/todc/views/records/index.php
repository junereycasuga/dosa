<title><?php echo $this->pageTitle; ?></title>
<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$('#classcards-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

$('.search-form form').submit(function(){
	$('#collectedIds-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="row-fluid">
	<div class="span12">
		<ul class="nav nav-tabs nav-tabs-google">
			<li class="active"><a href="#dropped" data-toggle="tab">Drop Class Cards</a></li>
			<li><a href="#collected" data-toggle="tab">Collected Student Ids</a></li>
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="dropped">
				<?php if(Users::model()->findByPk(Yii::app()->user->id)->user_type != 1){ ?>
					<a href="#dropClasscardModal" class="btn btn-danger" role="button" data-toggle="modal">Drop Classcard</a>
				<?php } ?>
				<?php $this->widget('zii.widgets.grid.CGridView',array(
				'id'=>'classcards-grid',
				'dataProvider'=>$classcards->search(),
				'filter'=>$classcards,
				'itemsCssClass'=>'table table-border table-hover',
				'columns'=>array(
					array(
						'name'=>'student_id',
						'type'=>'raw',
						'value'=>'$data->studentname',
						'filter'=>Students::getStudents()
					),
					array(
						'name'=>'subject_id',
						'type'=>'raw',
						'value'=>'$data->subjecttitle',
						'filter'=>Subjects::getSubjects()
					),
					'date_dropped',
					'date_claimed',
					array(
						'name'=>'status',
						'value'=>'($data->status == 0) ? "Not Claimed" : "Claimed"',
						'filter'=>CHtml::listData(Classcards::getClaimedUnclaimed(),'id','title')
					),
					array(
						'class'=>'CButtonColumn',
						'template'=>'{view} {update}',
						'buttons'=>array(
							'view'=>array(
								'url'=>'Yii::app()->createUrl("records/view", array("id"=>$data->id, "type"=>"classcard"))'
							),
							'update'=>array(
								'label'=>'Claim',
								'url'=>'Yii::app()->createUrl("records/claim", array("id"=>$data->id, "type"=>"classcard"))',
							),
						),
					),
				),
			)); ?>
			</div>

			<div class="tab-pane" id="collected">
				<a href="#collectIdModal" class="btn btn-danger" role="button" data-toggle="modal">Collect ID</a>
				<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'collectedIds-grid',
				'dataProvider'=>$collectedIds->search(),
				'filter'=>$collectedIds,
				'itemsCssClass'=>'table table-border table-hover',
				'columns'=>array(
					array(
						'name'=>'student_id',
						'type'=>'raw',
						'value'=>'$data->studentname',
						'filter'=>Students::getStudents()
					),
					'reason',
					'date_collected',
					'date_claimed',
					array(
						'name'=>'status',
						'value'=>'($data->status == 0) ? "Not Claimed" : "Claimed"',
						'filter'=>CHtml::listData(Classcards::getClaimedUnclaimed(),'id','title')
					),
					array(
						'class'=>'CButtonColumn',
						'template'=>'{view} {update}',
						'buttons'=>array(
							'view'=>array(
								'url'=>'Yii::app()->createUrl("records/view",array("id"=>$data->id,"type"=>"studentId"))'
							),
							'update'=>array(
								'label'=>'Claim',
								'url'=>'Yii::app()->createUrl("records/claim", array("id"=>$data->id, "type"=>"studentId"))',
							),
						),
					),
				),
			)); ?>
			</div>
		</div>
	</div>
</div>

<!-- drop classcard modal -->
<div class="modal hide fade" id="dropClasscardModal" tabindex="-1" role="dialog">
	<div class="modal-header">
		<button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Drop Classcard</h3>
	</div>
	<div class="modal-body">
		<?php $this->renderPartial('_dropForm',array('classcards'=>$classcards)); ?>
	</div>
</div>

<!-- collect id modal -->
<div class="modal hide fade" id="collectIdModal" tabindex="-1" role="dialog">
	<div class="modal-header">
		<button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Collect ID</h3>
	</div>
	<div class="modal-body">
		<?php $this->renderPartial('_collectForm',array('collectedIds'=>$collectedIds)); ?>
	</div>
</div>