<?php
/* @var $this StudentsController */
/* @var $model Students */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'students-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'student_firstname'); ?>
		<?php echo $form->textField($model,'student_firstname',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'student_firstname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'student_middlename'); ?>
		<?php echo $form->textField($model,'student_middlename',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'student_middlename'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'student_lastname'); ?>
		<?php echo $form->textField($model,'student_lastname',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'student_lastname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'student_course_id'); ?>
		<?php echo $form->textField($model,'student_course_id'); ?>
		<?php echo $form->error($model,'student_course_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'student_year_level'); ?>
		<?php echo $form->textField($model,'student_year_level'); ?>
		<?php echo $form->error($model,'student_year_level'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'student_address'); ?>
		<?php echo $form->textArea($model,'student_address',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'student_address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'student_email'); ?>
		<?php echo $form->textField($model,'student_email',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'student_email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'studetn_contact_no'); ?>
		<?php echo $form->textField($model,'studetn_contact_no',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'studetn_contact_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'guardian_name'); ?>
		<?php echo $form->textField($model,'guardian_name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'guardian_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'guardian_address'); ?>
		<?php echo $form->textField($model,'guardian_address',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'guardian_address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'guardian_contact_no'); ?>
		<?php echo $form->textField($model,'guardian_contact_no',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'guardian_contact_no'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->