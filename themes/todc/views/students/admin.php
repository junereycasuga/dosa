<title><?php echo $this->pageTitle; ?></title>
<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$('#students-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="row-fluid">
	<div class="span12">
		<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'students-grid',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'itemsCssClass'=>'table table-border table-hover',
		'columns'=>array(
			'id',
			'student_firstname',
			'student_lastname',
			array(
				'name'=>'student_course_id',
				'value'=>'$data->getCourse()',
				'header'=>'Course',
			),
			'student_year_level',
			/*
			'student_address',
			'studetn_contact_no',
			'guardian_name',
			'guardian_contact_no',
			*/
			array(
				'class'=>'CButtonColumn',
				'template'=>'{view}',
			),
		),
	)); ?>
	</div>
</div>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->


