<?php
/* @var $this StudentsController */
/* @var $data Students */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('student_firstname')); ?>:</b>
	<?php echo CHtml::encode($data->student_firstname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('student_middlename')); ?>:</b>
	<?php echo CHtml::encode($data->student_middlename); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('student_lastname')); ?>:</b>
	<?php echo CHtml::encode($data->student_lastname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('student_course_id')); ?>:</b>
	<?php echo CHtml::encode($data->student_course_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('student_year_level')); ?>:</b>
	<?php echo CHtml::encode($data->student_year_level); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('student_address')); ?>:</b>
	<?php echo CHtml::encode($data->student_address); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('studetn_contact_no')); ?>:</b>
	<?php echo CHtml::encode($data->studetn_contact_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guardian_name')); ?>:</b>
	<?php echo CHtml::encode($data->guardian_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guardian_contact_no')); ?>:</b>
	<?php echo CHtml::encode($data->guardian_contact_no); ?>
	<br />

	*/ ?>

</div>