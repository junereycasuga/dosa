<?php
/* @var $this StudentsController */
/* @var $model Students */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'student_firstname'); ?>
		<?php echo $form->textField($model,'student_firstname',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'student_middlename'); ?>
		<?php echo $form->textField($model,'student_middlename',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'student_lastname'); ?>
		<?php echo $form->textField($model,'student_lastname',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'student_course_id'); ?>
		<?php echo $form->textField($model,'student_course_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'student_year_level'); ?>
		<?php echo $form->textField($model,'student_year_level'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'student_address'); ?>
		<?php echo $form->textArea($model,'student_address',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'studetn_contact_no'); ?>
		<?php echo $form->textField($model,'studetn_contact_no',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'guardian_name'); ?>
		<?php echo $form->textField($model,'guardian_name',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'guardian_contact_no'); ?>
		<?php echo $form->textField($model,'guardian_contact_no',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->