<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- stylesheets -->
		<?php 
		Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/lib/css/bootstrap.css');
		Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/lib/css/bootstrap-responsive.css');
		Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/lib/css/todc-bootstrap.css');
		Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/lib/css/custom.css');
		Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/lib/css/select2.css');
		?>
	</head>
	<body>
		
		<?php
		if(!Yii::app()->user->isGuest){
			$this->renderPartial('//layouts/header');
		}
		?>
		
		<?php echo $content; ?>
			
		<!-- javascripts -->
		<?php
		if(Yii::app()->controller->id != 'users' || Yii::app()->controller->id != 'students' || Yii::app()->controller->id != 'records'){
			if(Yii::app()->controller->action->id != 'index'){
				Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/lib/js/jquery-1.10.2.min.js');
			}
		}
		Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/lib/js/bootstrap.min.js');
		Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/lib/js/select2.min.js');
		?>
	</body>
</html>