<div class=" navbar navbar-fixed-top">
	<div class="navbar-inverse">
		<div class="navbar-inner">
			<div class="container">
				<button class="btn btn-navbar" type="button" data-toggle="collapse" data-target=".nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon bar"></span>
				</button>
				<a class="brand" href="#"><?php echo Yii::app()->name; ?></a>
				<div class="nav-collapse collapse">
					<!-- <p class="navbar-text pull-right">
						Logged in as <a href="#" class="navbar-link">User</a>
					</p> -->
					<ul class="nav"> 
						<li><a href="<?php echo Yii::app()->createUrl('site/about'); ?>">Home</a></li>
						<?php if(Users::model()->findByPk(Yii::app()->user->id)->user_type == 1){ ?>
						<li><a href="<?php echo Yii::app()->createUrl('users/index'); ?>">Users</a></li>
						<li><a href="#">Students/Instructors</a></li>
						<!-- <li><a href="<?php echo Yii::app()->createUrl('students/index'); ?>">Student Profile</a></li> -->
						<li><a href="<?php echo Yii::app()->createUrl('records/index'); ?>">Student Records</a></li>
						<?php } else if(Users::model()->findByPk(Yii::app()->user->id)->user_type == 2){ ?>
						<li><a href="<?php echo Yii::app()->createUrl('subjects/index'); ?>">My Subjects</a></li>
						<li><a href="<?php echo Yii::app()->createUrl('students/index'); ?>">Student Profile</a></li>
						<li><a href="<?php echo Yii::app()->createUrl('records/index'); ?>">Student Records</a></li>
						<?php } ?>
						<?php if(Users::model()->findByPk(Yii::app()->user->id)->user_type == 3){ ?>
						<li><a href="<?php echo Yii::app()->createUrl('users/profile'); ?>">My Profile</a></li>
						<li><a href="<?php echo Yii::app()->createUrl('records/index'); ?>">Student Records</a></li>
						<?php } ?>
						</ul>

					<ul class="nav pull-right">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo  Yii::app()->user->userFirstname." ".Yii::app()->user->userLastname; ?> <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<!-- <li><a href="#">Settings</a></li> -->
								<li><a href="<?php echo Yii::app()->createUrl('site/logout'); ?>">Logout</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>	

	<div class="navbar-masthead">
		<div class="navbar-inner">
			<div class="container">
				<h4>
					<?php
					if(Yii::app()->controller->id == 'users'){
						if(Yii::app()->controller->action->id == 'index'){
							echo "Manage Users";
						} else if(Yii::app()->controller->action->id == 'view'){
							echo "User Details";
						}
					} else if(Yii::app()->controller->id == 'site'){
						if(Yii::app()->controller->action->id == 'about'){
							echo "About Student Affairs Office of Lorma Colleges";
						}
					} else if(Yii::app()->controller->id == 'students'){
						if(Yii::app()->controller->action->id == 'index'){
							echo "Manage Students";
						} else if(Yii::app()->controller->action->id == 'view'){
							echo "Student Details";
						}
					} else if(Yii::app()->controller->id == 'records'){
						if(Yii::app()->controller->action->id == 'index'){
							echo "Student Records";
						}
					}
					?>
				</h4>
			</div>
		</div>
	</div>
</div>


<!-- flash notification messages -->
<?php if(Yii::app()->user->hasFlash('msg')): ?>
	<div class="<?php echo Yii::app()->user->getFlash('msgClass'); ?>">
		<button class="close" type="button" data-dismiss="alert">&times;</button>
		<?php echo Yii::app()->user->getFlash('msg'); ?>
	</div>
<?php endif; ?>