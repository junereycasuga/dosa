<title><?php echo $this->pageTitle; ?></title>
<div class="signin">
	<div class="row-fluid">
		<div class="jumbotron span5">
			<h1>DOSA</h1>
		</div>
		
		<div class="signin-box span7">
			<h2 class="form-signin-heading">Sign in</h2>
			<?php if(Yii::app()->user->hasFlash('msg')): ?>
			<div class="<?php echo Yii::app()->user->getFlash('msgClass'); ?>">
				<button class="close" type="button" data-dismiss="alert">&times;</button>
				<?php echo Yii::app()->user->getFlash('msg'); ?>
			</div>
			<?php endif; ?>
			<?php $form = $this->beginWidget('CActiveForm',array(
			'id'=>'login-form',
			'enableClientValidation'=>true,
			'enableAjaxValidation'=>false,
			)); ?>
			<fieldset>
				<label for="username"><?php echo $form->label($model,'username'); ?></label>
				<?php echo $form->textField($model,'username',array('id'=>'username','class'=>'input-block-level')); ?>

				<label for="password"><?php echo $form->label($model,'user_password'); ?></label>
				<?php echo $form->passwordField($model,'user_password',array('id'=>'password','class'=>'input-block-level')); ?>

				<input type="submit" value="Sign in" class="btn btn-primary" name="btnLogin">
				<label class="remember">
					<input type="checkbox" name="Users[rememberMe]" value="yes">
					<strong class="remember-label">Stay signed in</strong>
				</label>
			</fieldset>
			<?php $this->endWidget(); ?>
		</div>
	</div>
</div>