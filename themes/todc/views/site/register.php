<?php $form = $this->beginWidget('CActiveForm', array(
	'id'=>'addUser-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		),
	)); ?>

	<?php echo $form->textField($model,'username',array('placeholder'=>'username')); ?>
	<?php echo $form->passwordField($model,'user_password',array('placeholder'=>'password')); ?>
	<?php echo $form->textField($model,'user_firstname', array('placeholder'=>'first name')); ?>
	<?php echo $form->textField($model,'user_lastname', array('placeholder'=>'last name')); ?>
	<?php echo $form->textField($model,'user_type', array('placeholder'=>'type')); ?>

	<input type="submit" value="Save" name="btnRegister">

<?php $this->endWidget(); ?>