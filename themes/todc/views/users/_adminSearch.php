<?php
/* @var $this AdminController */
/* @var $model Users */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'htmlOptions'=>array(
		'class'=>'form-horizontal',
	),
)); ?>
	
	<div class="control-group">
		<?php echo $form->label($model,'id',array('class'=>'control-label')); ?>
		<div class="controls">
			<?php echo $form->textField($model,'id'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->label($model,'username',array('class'=>'control-label')); ?>
		<div class="controls">
			<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>100)); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->label($model,'user_status', array('class'=>'control-label')); ?>
		<div class="controls">
			<?php echo $form->textField($model,'user_status'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->label($model,'user_created_at',array('class'=>'control-label')); ?>
		<div class="controls">
			<?php echo $form->textField($model,'user_created_at'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->label($model,'user_type',array('class'=>'control-label')); ?>
		<div class="controls">
			<?php echo $form->textField($model,'user_type'); ?>
		</div>
	</div>

	<div class="control-group">
		<div class="controls">
			<input type="submit" value="Search" class="btn btn-primary">
		</div>
	</div>
<?php $this->endWidget(); ?>

<!-- search-form -->