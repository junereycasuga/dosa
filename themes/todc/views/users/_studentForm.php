<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'instructor-user-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal',
	),
)); ?>
	<?php echo $form->errorSummary($modelUser); ?>
	<?php echo $form->errorSummary($modelStudent); ?>

	<div class="control-group">
		<label class="control-label">Instructor *</label>
		<div class="controls">
			<?php echo $form->dropDownList($modelUser,'employee_id', Students::getStudents(),array('prompt'=>'Select Student')); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($modelUser,'username',array('class'=>'control-label')); ?>
		<div class="controls">
			<?php echo $form->textField($modelUser,'username'); ?>
			<?php echo $form->error($modelUser,'username'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($modelUser,'user_password',array('class'=>'control-label')); ?>
		<div class="controls">
			<?php echo $form->passwordField($modelUser,'user_password'); ?>
			<?php echo $form->error($modelUser,'user_password'); ?>
		</div>
	</div>

	<div class="control-group">
		<div class="controls">
			<?php echo CHtml::submitButton($modelUser->isNewRecord ? 'Create' : 'Save', array('class'=>'btn btn-primary', 'name'=>'btnSaveStudent')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>