<title><?php echo $this->pageTitle; ?></title>
<?php
/* @var $this AdminController */
/* @var $model Users */
?>

<div class="row-fluid">
	<div class="span12">
		<div class="navbar navbar-toolbar">
			<div class="navbar-inner">
				<div class="btn-toolbar">
					<div class="btn-group">
						<a href="<?php echo Yii::app()->createUrl('users/index'); ?>" class="btn" role="button">Back</a>
					</div>
					<div class="btn-group">
						<a href="<?php echo Yii::app()->createUrl('users/delete',array('id'=>$model->id)); ?>" class="btn btn-danger" role="button">Delete</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row-fluid">
	<div class="span6">
		<?php $form=$this->beginWidget('CActiveForm',array(
			'id'=>'update-user-form',
			'enableAjaxValidation'=>false,
			'htmlOptions'=>array(
				'class'=>'form-horizontal',
			),
		)); ?>
			<div class="control-group">
				<label class="control-label">Employee ID</label>
				<div class="controls">
					<input type="text" value="<?php echo $model->employee_id; ?>">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Name</label>
				<div class="controls">
					<input type="text" value="<?php echo $userName;?>">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Username</label>
				<div class="controls">
					<input type="text" value="<?php echo $model->username; ?>">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">User Type</label>
				<div class="controls">
					<input type="text" value="<?php echo $userType; ?>">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Created At</label>
				<div class="controls">
					<input type="text" value="<?php echo $model->user_created_at; ?>">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">User Status</label>
				<div class="controls">
					<?php echo $form->dropDownList($model,'user_status', array('1'=>'Active','0'=>'Inactive'), array('id'=>'statusList')); ?>
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<input type="submit" value="Save Changes" class="btn btn-danger" id="btn_Save" name="btnSave">
				</div>
			</div>
		<?php $this->endWidget(); ?>
	</div>
</div>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/lib/js/jquery-1.10.2.min.js'); ?>