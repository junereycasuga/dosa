<div class="row">
	<div class="span6">
		<h4>Add Student</h4>
		<?php $studentForm=$this->beginWidget('CActiveForm', array(
			'id'=>'student-form',
			'enableAjaxValidation'=>false,
			'htmlOptions'=>array(
				'class'=>'form-horizontal',
			),
		)); ?>
		<div class="control-group">
			<?php echo $studentForm->labelEx($modelStudent,'student_firstname',array('class'=>'control-label')); ?>
			<div class="controls">
				<?php echo $studentForm->textField($modelStudent,'student_firstname'); ?>
			</div>
		</div>

		<div class="control-group">
			<?php echo $studentForm->labelEx($modelStudent,'student_middlename', array('class'=>'control-label')); ?>
			<div class="controls">
				<?php echo $studentForm->textField($modelStudent,'student_middlename'); ?>
			</div>
		</div>

		<div class="control-group">
			<?php echo $studentForm->labelEx($modelStudent,'student_lastname', array('class'=>'control-label')); ?>
			<div class="controls">
				<?php echo $studentForm->textField($modelStudent,'student_lastname'); ?>
			</div>
		</div>

		<div class="control-group">
			<?php echo $studentForm->labelEx($modelStudent,'student_course_id',array('class'=>'control-label')); ?>
			<div class="controls">
				<?php echo $studentForm->dropDownList($modelStudent,'student_course_id', Courses::getCourses()); ?>
			</div>
		</div>

		<div class="control-group">
			<?php echo $studentForm->labelEx($modelStudent,'student_year_level',array('class'=>'control-label')); ?>
			<div class="controls">
				<?php echo $studentForm->dropDownList($modelStudent,'student_year_level',array('1'=>'1st year', '2'=>'2nd year','3'=>'3rd year','4'=>'4th year','5'=>'5th year')); ?>
			</div>
		</div>

		<div class="control-group">
			<?php echo $studentForm->labelEx($modelStudent,'student_address', array('class'=>'control-label')); ?>
			<div class="controls">
				<?php echo $studentForm->textArea($modelStudent,'student_address'); ?>
			</div>
		</div>

		<div class="control-group">
			<?php echo $studentForm->labelEx($modelStudent,'student_email', array('class'=>'control-label')); ?>
			<div class="controls">
				<?php echo $studentForm->textField($modelStudent,'student_email'); ?>
			</div>
		</div>

		<div class="control-group">
			<?php echo $studentForm->labelEx($modelStudent,'studetn_contact_no',array('class'=>'control-label')); ?>
			<div class="controls">
				<?php echo $studentForm->textField($modelStudent,'studetn_contact_no'); ?>
			</div>
		</div>

		<div class="control-group">
			<?php echo $studentForm->labelEx($modelStudent,'guardian_name',array('class'=>'control-label')); ?>
			<div class="controls">
				<?php echo $studentForm->textField($modelStudent,'guardian_name'); ?>
			</div>
		</div>

		<div class="control-group">
			<?php echo $studentForm->labelEx($modelStudent,'guardian_email', array('class'=>'control-label')); ?>
			<div class="controls">
				<?php echo $studentForm->textField($modelStudent,'guardian_email'); ?>
			</div>
		</div>

		<div class="control-group">
			<?php echo $studentForm->labelEx($modelStudent,'guardian_contact_no',array('class'=>'control-label')); ?>
			<div class="controls">
				<?php echo $studentForm->textField($modelStudent,'guardian_contact_no'); ?>
			</div>
		</div>

		<div class="control-group">
			<div class="controls">
				<?php echo CHtml::submitButton('Save',array('class'=>'btn btn-danger','name'=>'btn_savestudent')); ?>
			</div>
		</div>

		<?php $this->endWidget(); ?>
	</div>
	<div class="span6">
		<h4>Add Instructor</h4>
		<?php $instructorForm=$this->beginWidget('CActiveForm', array(
			'id'=>'instructor-form',
			'enableAjaxValidation'=>false,
			'htmlOptions'=>array(
				'class'=>'form-horizontal',
			),
		)); ?>
		<div class="control-group">
			<?php echo $instructorForm->labelEx($modelInstructor,'instructor_firstname',array('class'=>'control-label')); ?>
			<div class="controls">
				<?php echo $instructorForm->textField($modelInstructor,'instructor_firstname'); ?>
			</div>
		</div>

		<div class="control-group">
			<?php echo $instructorForm->labelEx($modelInstructor,'instructor_lastname', array('class'=>'control-label')); ?>
			<div class="controls">
				<?php echo $instructorForm->textField($modelInstructor,'instructor_lastname'); ?>
			</div>
		</div>

		<div class="control-group">
			<?php echo $instructorForm->labelEx($modelInstructor,'instructor_email', array('class'=>'control-label')); ?>
			<div class="controls">
				<?php echo $instructorForm->textField($modelInstructor,'instructor_email'); ?>
			</div>
		</div>

		<div class="control-group">
			<div class="controls">
				<?php echo CHtml::submitButton('Save',array('class'=>'btn btn-danger','name'=>'btn_saveinstructor')); ?>
			</div>
		</div>
		<?php $this->endWidget(); ?>
	</div>
</div>
