<?php
/* @var $this AdminController */
/* @var $model Users */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'admin-user-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal',
	),
)); ?>

	<?php echo $form->errorSummary($modelUser); ?>

	<div class="control-group">
		<?php echo $form->labelEx($modelUser,'username',array('class'=>'control-label')); ?>
		<div class="controls">
			<?php echo $form->textField($modelUser,'username'); ?>
			<?php echo $form->error($modelUser,'username'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($modelUser,'user_password',array('class'=>'control-label')); ?>
		<div class="controls">
			<?php echo $form->passwordField($modelUser,'user_password'); ?>
			<?php echo $form->error($modelUser,'user_password'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($modelUser,'employee_id', array('class'=>'control-label')); ?>
		<div class="controls">
			<?php echo $form->textField($modelUser,'employee_id'); ?>
			<?php echo $form->error($modelUser,'employee_id'); ?>
		</div>
	</div>

	<div class="control-group">
		<div class="controls">
			<?php echo CHtml::submitButton($modelUser->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-danger','name'=>'btnSaveAdmin')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>
<!-- form -->