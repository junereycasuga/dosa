<title><?php echo $this->pageTitle; ?></title>
<?php 
Yii::app()->clientScript->registerScript('search', "
	$('.search-form form').submit(function(){
		$('#users-grid').yiiGridView('update', {
			data: $(this).serialize()
		});
		return false;
	});
")
?>
<div class="row-fluid">
	<a href="#addUserModal" class="btn btn-danger" role="button" data-toggle="modal">Add User</a>
	<div class="span12">
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'users-grid',
			'dataProvider'=>$modelUser->search(),
			'filter'=>$modelUser,
			'itemsCssClass'=>'table table-border table-hover',
			'columns'=>array(
				'employee_id',
				'username',
				array(
					'header'=>'Employee Name',
					'name'=>'employee_id',
					'type'=>'raw',
					'value'=>'$data->usersname',
				),
				array(
					'name'=>'user_type',
					'value'=>'($data->user_type == 1) ? "Administrator" : "Instructor"',
				),
				'user_created_at',
				array(
					'name'=>'user_status',
					'value'=>'($data->user_status == 0) ? "Inactive" : "Acitve"',
					'filter'=>CHtml::listData(array(
						array('id'=>'0','title'=>'Inactive'),
						array('id'=>'1','title'=>'Acitve')),'id','title'),
				),
				
				array(
					'class'=>'CButtonColumn',
					'template'=>'{view} {delete}',
				),
			),
		)); ?>
	</div>

	<div class="modal hide fade" id="addUserModal" tabindex="-1" role="dialog">
		<div class="modal-header">
			<button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Add User</h3>
		</div>
		<div class="modal-body">
			<div class="row-fluid">
				<div class="span4">
					<a href="#addUserAdminModal" class="btn btn-danger btn-large" role="button" data-toggle="modal" data-dismiss="modal">Add Admin User</a>
				</div>
				<div class="span4">
					<a href="#addUserInstructorModal" class="btn btn-danger btn-large" role="button" data-toggle="modal" data-dismiss="modal">Add Instructor User</a>
				</div>
				<div class="span4">
					<a href="#addUserStudentModal" class="btn btn-danger btn-large" role="button" data-toggle="modal" data-dismiss="modal">Add Student User</a>
				</div>
			</div>
		</div>
	</div>

	<!-- add admin user modal -->
	<div class="modal hide fade" id="addUserAdminModal" tabindex="-1" role="dialog">
		<div class="modal-header">
			<button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Add User</h3>
		</div>
		<div class="modal-body">
			<?php $this->renderPartial('_adminForm',array('modelAdmin'=>$modelAdmin,'modelUser'=>$modelUser)); ?>
		</div>
	</div>

	<!-- add instructor user modal -->
	<div class="modal hide fade" id="addUserInstructorModal" tabindex="-1" role="dialog">
		<div class="modal-header">
			<button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Add User</h3>
		</div>
		<div class="modal-body">
			<?php $this->renderPartial('_instructorForm',array('modelInstructor'=>$modelInstructor,'modelUser'=>$modelUser)); ?>
		</div>
	</div>
	
	<!-- add student user modal -->
	<div class="modal hide fade" id="addUserStudentModal" tabindex="-1" role="dialog">
		<div class="modal-header">
			<button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Add User</h3>
		</div>
		<div class="modal-body">
			<?php $this->renderPartial('_studentForm',array('modelStudent'=>$modelStudent,'modelUser'=>$modelUser)); ?>
		</div>
	</div>
</div>