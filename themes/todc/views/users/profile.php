<?php
/* @var $this StudentsController */
/* @var $model Students */
?>
<title><?php echo $this->pageTitle; ?></title>
<div class="row-fluid">
	<div class="span6">
		<table class="table table-striped">
			<tbody>
				<tr>
					<th>Sudent ID</th>
					<td><?php echo $model->id; ?></td>
				</tr>
				<tr>
					<th>Name</th>
					<td><?php echo $model->student_firstname . " " . $model->student_lastname; ?></td>
				</tr>
				<tr>
					<th>Course</th>
					<td><?php echo Courses::getCourse($model->student_course_id); ?></td>
				</tr>
				<tr>
					<th>Year Level</th>
					<td><?php echo Students::getYearLevel($model->student_year_level); ?></td>
				</tr>
				<tr>
					<th>Address</th>
					<td><?php echo $model->student_address; ?></td>
				</tr>
				<tr>
					<th>Email</th>
					<td><?php echo $model->student_email; ?></td>
				</tr>
				<tr>
					<th>Contact Number</th>
					<td><?php echo $model->studetn_contact_no; ?></td>
				</tr>
				<tr>
					<th>Guardian Name</th>
					<td><?php echo $model->guardian_name; ?></td>
				</tr>
				<tr>
					<th>Guardian Contact Number</th>
					<td><?php echo $model->guardian_contact_no; ?></td>
				</tr>
			</tbody>
		</table>
	</div>
	<?php if(Users::model()->findByPk(Yii::app()->user->id)->user_type == 1){ ?>
	<div class="span6">
		<h4>User Credentials</h4>
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th>Username</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><?php echo Users::getUsername($model->id,3); ?></td>
				</tr>
			</tbody>
		</table>
		<h4>Enrolled Subjects</h4>
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th>Subject</th>
					<!-- <th>Instructor</th> -->
				</tr>
			</thead>
			<tbody>
				<?php foreach($subject as $s){ ?>
				<tr>
					<td><?php echo Subjects::getSubjectTitle($s['subject_id']); ?></td>
					<!-- <td></td> -->
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	<?php } else if(Users::model()->findByPk(Yii::app()->user->id)->user_type == 3){ ?>
	<div class="span6">
		<h4>Dropped Classcards</h4>
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th>Subject</th>
					<th>No. of Drops</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($studentSubjects as $key=>$value){ ?>
					<tr>
						<td><?php echo $value['subject_title']; ?></td>
						<td><?php echo Classcards::countDropsOfAStudentPerSubject($model->id, $value['student_id']); ?></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>

	<?php } ?>
</div>
