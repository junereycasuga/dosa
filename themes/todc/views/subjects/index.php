<title><?php echo $this->pageTitle; ?></title>
<div class="row-fluid">
	<div class="span5">
		<table class="table table-border table-hover">
			<thead>
				<tr>
					<th>Subjects</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($instructorSubjects as $iS){ ?>
				<tr>
					<td><a href="<?php echo Yii::app()->createUrl('subjects/index',array('subjectId'=>$iS["id"])); ?>"><?php echo Subjects::getSubjectTitle($iS['id']); ?></a></td>
					
				</tr>
				<?php } ?>
			</tbody>
		</table>

		<div class="well well-small">
			<b>*NOTE:</b> Select a subject to see class roster.
		</div>
	</div>

	<div class="span7">
	<?php if(isset($_GET['subjectId'])){ ?>
		<div class="well">
			<h3><?php echo Subjects::getSubjectTitle($_GET['subjectId']); ?></h3>
			<table class="table table-hover table-border">
				<thead>
					<tr>
						<th>Students</th>
						<th>Status</th>
						<th>No. of Drops</th>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($students as $stud){ ?>
					<tr>
						<td><a href="<?php echo Yii::app()->createUrl('students/view',array('id'=>$stud['student_id'])); ?>"><?php echo $studentsModel->getStudentName($stud['student_id']); ?></a></td>
						<td>
						<?php
						$stat = Classcards::checkIfStudentIsDropped($stud['student_id'], $_GET['subjectId']);
						echo $stat;
						?>
						</td>
						<td><?php
						echo Classcards::countDropsOfAStudentPerSubject($stud['student_id'], $_GET['subjectId']);
						?></td>
						<td>
						<?php if($stat == "Dropped"){ ?>
							<a href="<?php echo Yii::app()->createUrl('subjects/claimclasscard',array('studentId'=>$stud['student_id'], 'subjectId'=>$_GET['subjectId'])); ?>" class="btn btn-danger btn-small" role="button">Claim</a>
						<?php } else { ?>
							<a href="<?php echo Yii::app()->createUrl('subjects/dropstudent', array('studentId'=>$stud['student_id'],'subjectId'=>$_GET['subjectId'])); ?>" class="btn btn-danger btn-small" role="button">Drop</a>
						<?php } ?>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	<?php } ?>
	</div>
</div>