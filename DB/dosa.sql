-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 09, 2014 at 05:59 PM
-- Server version: 5.5.35
-- PHP Version: 5.3.10-1ubuntu3.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dosa`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrators`
--

CREATE TABLE IF NOT EXISTS `administrators` (
  `id` int(11) NOT NULL DEFAULT '0',
  `admin_firstname` varchar(100) DEFAULT NULL,
  `admin_lastname` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrators`
--

INSERT INTO `administrators` (`id`, `admin_firstname`, `admin_lastname`) VALUES
(2800296, 'Super', 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `classcards`
--

CREATE TABLE IF NOT EXISTS `classcards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `date_dropped` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_claimed` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0=dropped; 1=claimed;',
  PRIMARY KEY (`id`),
  KEY `classcards_instructor_subjects_FK` (`subject_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `classcards`
--

INSERT INTO `classcards` (`id`, `student_id`, `subject_id`, `date_dropped`, `date_claimed`, `status`) VALUES
(1, 2, 1, '2013-09-09 14:46:35', '2013-09-15 11:01:02', 1),
(2, 1, 1, '2013-09-15 10:53:24', '2013-09-15 11:01:19', 1),
(3, 2, 2, '2013-09-15 11:01:12', '2013-09-15 17:04:42', 1);

-- --------------------------------------------------------

--
-- Table structure for table `collected_id`
--

CREATE TABLE IF NOT EXISTS `collected_id` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `reason` text COMMENT 'reason why student id was collected',
  `date_collected` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_claimed` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` int(11) DEFAULT '0' COMMENT '0=collected; 1=claimed;',
  PRIMARY KEY (`id`),
  KEY `collected_id_students_FK` (`student_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `collected_id`
--

INSERT INTO `collected_id` (`id`, `student_id`, `reason`, `date_collected`, `date_claimed`, `status`) VALUES
(1, 1, 'not wearing uniform', '2013-09-09 14:58:29', '2013-09-10 15:35:14', 0),
(2, 2, 'test reason', '2013-09-09 16:10:04', '2013-09-10 16:22:52', 0);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_shortcode` varchar(50) NOT NULL,
  `course_title` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `courses_PK` (`course_shortcode`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `course_shortcode`, `course_title`) VALUES
(1, 'BSIT', 'Bachelor of Science in Information Technology'),
(2, 'BSCS', 'Bachelor of Science in Computer Science');

-- --------------------------------------------------------

--
-- Table structure for table `instructors`
--

CREATE TABLE IF NOT EXISTS `instructors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instructor_firstname` varchar(100) DEFAULT NULL,
  `instructor_lastname` varchar(100) DEFAULT NULL,
  `instructor_email` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2800300 ;

--
-- Dumping data for table `instructors`
--

INSERT INTO `instructors` (`id`, `instructor_firstname`, `instructor_lastname`, `instructor_email`) VALUES
(2800297, 'Junerey', 'Casuga', '0'),
(2800298, 'Ellen', 'Mangaoang', '0'),
(2800299, 'Teacher', 'Instructor', '0');

-- --------------------------------------------------------

--
-- Table structure for table `instructor_subjects`
--

CREATE TABLE IF NOT EXISTS `instructor_subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instructor_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `instructor_subjects_subjects_FK` (`subject_id`),
  KEY `instructor_subjects_instructors_FK` (`instructor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `instructor_subjects`
--

INSERT INTO `instructor_subjects` (`id`, `instructor_id`, `subject_id`) VALUES
(1, 2800297, 1),
(2, 2800297, 2),
(3, 2800298, 5);

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_firstname` varchar(100) NOT NULL,
  `student_middlename` varchar(100) NOT NULL,
  `student_lastname` varchar(100) NOT NULL,
  `student_course_id` int(11) NOT NULL,
  `student_year_level` int(11) NOT NULL,
  `student_address` text,
  `student_email` varchar(250) NOT NULL,
  `studetn_contact_no` varchar(100) DEFAULT NULL,
  `guardian_name` varchar(100) DEFAULT NULL,
  `guardian_email` varchar(250) NOT NULL,
  `guardian_contact_no` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `students_courses_FK` (`student_course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `student_firstname`, `student_middlename`, `student_lastname`, `student_course_id`, `student_year_level`, `student_address`, `student_email`, `studetn_contact_no`, `guardian_name`, `guardian_email`, `guardian_contact_no`) VALUES
(1, 'Nino', 'Vincent', 'Padama', 1, 4, 'Balaoan', 'nino@gmail.com', '123', 'Nino Padama Sr.', '', '321'),
(2, 'Divina', 'P', 'Pis-ing', 2, 4, 'La Union', 'divina@yahoo.com', '312', 'Ellen Mangaoang', '', '3123');

-- --------------------------------------------------------

--
-- Table structure for table `student_subjects`
--

CREATE TABLE IF NOT EXISTS `student_subjects` (
  `student_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  KEY `student_subjects_instructor_subjects_FK` (`subject_id`),
  KEY `student_subjects_students_FK` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_subjects`
--

INSERT INTO `student_subjects` (`student_id`, `subject_id`) VALUES
(2, 1),
(2, 3),
(1, 2),
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE IF NOT EXISTS `subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_shortcode` varchar(50) NOT NULL,
  `subject_title` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `subjects_PK` (`subject_shortcode`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `subject_shortcode`, `subject_title`) VALUES
(1, 'COMPRO1', 'Ruby on Rails'),
(2, 'COMPRO2', 'Intro to TDD'),
(3, 'ENG1', 'English'),
(4, 'MATH1', 'Calculus'),
(5, 'COMPRO3', 'Java');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `user_password` varchar(300) DEFAULT NULL,
  `user_status` int(11) DEFAULT NULL COMMENT '0=inactive; 1=active',
  `user_type` int(11) DEFAULT NULL COMMENT '1=administrator; 2=instructor; 3=student',
  `user_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_PK` (`employee_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `employee_id`, `username`, `user_password`, `user_status`, `user_type`, `user_created_at`) VALUES
(1, 2800296, 'admin', 'c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec', 1, 1, '2013-09-02 13:19:52'),
(2, 2800297, 'junereycasuga', '0655a949c0826968e2daa324026f7b2dcccd94ac882ba5a4d5cec0fbc1fc0ff545a1a3b77560b7ead1704ca9f41b73a91c7d238f946f723e66fa96a17f1c3558', 1, 2, '2013-09-02 17:04:41'),
(5, 2800298, 'emangaoang', '2290fe866fd5847cbe83286a5f85871f7c125c421c8f6be125abcb4f963f1e60dadb58be308209359afd0dfdbb78a5a670543f746794c5edf9d300cb00f09d2c', 1, 2, '2013-09-15 10:03:46'),
(6, 1, 'nino', '1ab7580e8aebdd3f446997ee40494f059eaab6e7e19477d1630db7e4135290263782a2cdd04cb1715c93fc60f6a5ccb3de8949bba6375748c082ec12b4e705b4', 1, 3, '2014-01-12 16:15:13'),
(7, 2, 'divina', 'b7acfa5fc106ff2536e3c32ea730bd5e351f6ece4280139d0cf01d0419a49f75ef3f5bf43439723e8695bd05473a25df11e349ae78e5190fb2cc2923d9949602', 1, 3, '2014-01-12 16:29:26');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `collected_id`
--
ALTER TABLE `collected_id`
  ADD CONSTRAINT `collected_id_students_FK` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`);

--
-- Constraints for table `instructor_subjects`
--
ALTER TABLE `instructor_subjects`
  ADD CONSTRAINT `instructor_subjects_instructors_FK` FOREIGN KEY (`instructor_id`) REFERENCES `instructors` (`id`),
  ADD CONSTRAINT `instructor_subjects_subjects_FK` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`);

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `students_courses_FK` FOREIGN KEY (`student_course_id`) REFERENCES `courses` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
